# tal&dev full-stack coding challenge: Todo app

## INTRO
We have found your profile very interesting and, in order to understand if you’re a good fit for us, we would like you to show us what you do best: creating a web app!
Please, make sure you read the requirements very carefully, and start coding!
Also, don’t forget to read the “assignment evaluation criteria” paragraph, that explains what we will focus on to grade your work.
If you have any questions don’t be afraid to contact us.

## ASSIGNMENT  
Your task is to create a todo app.  
The app will allow users to:  
- add a tast to the list
- delete a task
- update a task
- add a subtask to a task
- delete a subtask from a task
- mark a subtask as completed
- mark a task as completed, only if all subtasks are completed

## BACK END
We ask you to use Hasura (www.hasura.io) to build the main service, using it to build a GraphQL API.  
You should use the default CRUD Hasura offers you.
The mutation that marks the task as completed must be built as an action (https://hasura.io/docs/latest/graphql/core/actions/index.html) and must check if all the subtasks are completed before saving the task as completed.
The action microservice must be built using Node.js and Typescript.

## FRONT END 
The frontend must be build using Typescript with React.js.
Feel free to design the app the way you want.  

## EVALUATION
We are not evaluating the perfectness of every piece but tests are expected, as is well written, simple idiomatic code.
You have to provide us something working and self contained, but at the same time we’d like you to think about:
- Test
- Architecture
- Code quality
- Maintainability 
- Documentation 
- UX/UI

## SUBMISSION
Your solution should be sent as a .git repository where you have saved all your work.  
All parts of the application must be Dockerized.  
Please insert as well a readme file where you explain briefly how to run everything. We are expecting to run your solution and test it live.

### Have fun coding!

