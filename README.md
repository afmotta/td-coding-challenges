# tal&dev Coding challenges

This repo contains all of the coding challenges created by the tal&dev team.

[Full-stack](full-stack.md)

[Frontend](front.md)