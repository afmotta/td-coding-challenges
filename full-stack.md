# tal&dev full-stack coding challenge

## INTRO
We have found your profile very interesting and, in order to understand if you’re a good fit for us, we would like you to show us what you do best: creating a web app!
Please, make sure you read the requirements very carefully, and start coding!
Also, don’t forget to read the “assignment evaluation criteria” paragraph, that explains what we will focus on to grade your work.
If you have any questions don’t be afraid to contact us.

## ASSIGNMENT
Your task is to create a dashboard that can be used internally to show a a list of users and their detail.
The dashboard is composed by a list of users. Clicking on a user will a detail page for the user.
Feel free to design the page the way you want. 

## BACK END
Please use Typescript.
You need to create a simple API server, serving two endpoints:
- get a list of all users
- get a user detail
  
For the data source, use the MOCK_DATA.json file.

## FRONT END 
Please for the front end use Typescript with React.js.
Feel free to use any tool (Bower, CommonJS, Grunt, Gulp, NPM, Webpack,...).
### IMPORTANT:

- No Boilerplate
- ES6 is mandatory

## EVALUATION
We are not evaluating the perfectness of every piece but tests are expected, as is well written, simple idiomatic code.
You have to provide us something working and self contained, but at the same time we’d like you to think about:
- Test
- Architecture
- Code quality
- Maintainability 
- Documentation 
- UX/UI

## SUBMISSION
Your solution should be sent as a .git repository where you have saved all your work. Please insert as well a readme file where you explain briefly how to run everything. We are expecting to run your solution and test it live.
Bonus point if you can submit a dockerized version of your work. Have fun coding!